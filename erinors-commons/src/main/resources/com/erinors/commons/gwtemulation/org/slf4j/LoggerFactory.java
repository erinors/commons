/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package org.slf4j;

import java.util.HashMap;
import java.util.Map;

public class LoggerFactory
{
    private static Map<String, Logger> loggers = new HashMap<String, Logger>();

    public static Logger getLogger(Class clazz)
    {
        return getLogger(clazz.getName());
    }

    public static Logger getLogger(String name)
    {
        Logger logger = loggers.get(name);
        if (logger == null)
        {
            logger = new LoggerImpl(name);
            loggers.put(name, logger);
        }

        return logger;
    }
}
