/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package org.slf4j;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

public class LoggerImpl implements Logger
{
    private final String name;

    public LoggerImpl(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public boolean isTraceEnabled()
    {
        return false;
    }

    @Override
    public void trace(String message)
    {
        trace(message, null);
    }

    @Override
    public void trace(String message, Throwable t)
    {
        // TODO log(Level.TRACE, name, message, t);
        log(null, name, message, t);
    }

    @Override
    public boolean isDebugEnabled()
    {
        return false;
    }

    @Override
    public void debug(String message)
    {
        debug(message, (Throwable) null); // TODO call log directly
    }

    @Override
    public void debug(String message, Throwable t)
    {
        // TODO log(Level.DEBUG, name, message, t);
        log(null, name, message, t);
    }

    @Override
    public void debug(String format, Object... arguments)
    {
        String message = format;

        for (Object argument : arguments)
        {
            message = message.replaceFirst("\\{\\}", argument != null ? argument.toString() : "null"); // TODO util
        }

        log(null, name, message, null);
    }

    @Override
    public boolean isInfoEnabled()
    {
        return true;
    }

    @Override
    public void info(String message)
    {
        info(message, null);
    }

    @Override
    public void info(String message, Throwable t)
    {
        // TODO log(Level.INFO, name, message, t);
        log(null, name, message, t);
    }

    @Override
    public boolean isWarnEnabled()
    {
        return true;
    }

    @Override
    public void warn(String message)
    {
        warn(message, null);
    }

    @Override
    public void warn(String message, Throwable t)
    {
        // TODO log(Level.WARN, name, message, t);
        log(null, name, message, t);
    }

    @Override
    public boolean isErrorEnabled()
    {
        return true;
    }

    @Override
    public void error(String message)
    {
        error(message, null);
    }

    @Override
    public void error(String message, Throwable t)
    {
        // TODO log(Level.ERROR, name, message, t);
        log(null, name, message, t);
    }

    private void log(Object level, String category, String message, Throwable t)
    {
        String finalMessage = category + " " + message;

        try
        {
            GWT.log(finalMessage);
        }
        catch (Throwable e)
        {
            // Ignore
        }

        try
        {
            System.out.println(finalMessage);
        }
        catch (Throwable e)
        {
            // Ignore
        }
    }
}
