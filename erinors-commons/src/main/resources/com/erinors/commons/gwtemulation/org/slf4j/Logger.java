/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package org.slf4j;

public interface Logger {
    String getName();
    
    boolean isTraceEnabled();
    
    void trace(String message);
    
    void trace(String message, Throwable t);

    boolean isDebugEnabled();
    
    void debug(String message);
    
    void debug(String message, Throwable t);

    void debug(String format, Object... arguments);

    boolean isInfoEnabled();

    void info(String message);
    
    void info(String message, Throwable t);

    boolean isWarnEnabled();

    void warn(String message);
    
    void warn(String message, Throwable t);

    boolean isErrorEnabled();

    void error(String message);
    
    void error(String message, Throwable t);
}
