/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

import java.util.Iterator;
import java.util.Set;

public interface JsCompatibleStringSet
{
    void add(String element);

    boolean contains(String element);

    void remove(String element);

    Set<String> asSet();

    Iterator<String> iterator();
}
