/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

import java.util.Iterator;
import java.util.Map;

public interface JsCompatibleMap
{
    void put(String key);

    void remove(String key);

    boolean contains(String key);

    Object get(String key);

    boolean getBoolean(String key);

    byte getByte(String key);

    char getChar(String key);

    double getDouble(String key);

    float getFloat(String key);

    int getInt(String key);

    long getLong(String key);

    short getShort(String key);

    JsCompatibleStringSet keys();

    Map<String, Object> asMap();

    Iterator<String> keyIterator();

    Iterator<Object> valueIterator();
}
