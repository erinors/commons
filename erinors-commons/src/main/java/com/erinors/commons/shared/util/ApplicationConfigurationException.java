package com.erinors.commons.shared.util;

/**
 * Thrown in case of a non-recoverable application configuration error.
 */
@SuppressWarnings("serial")
public class ApplicationConfigurationException extends RuntimeException
{
    public ApplicationConfigurationException()
    {
    }

    public ApplicationConfigurationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ApplicationConfigurationException(String message)
    {
        super(message);
    }

    public ApplicationConfigurationException(Throwable cause)
    {
        super(cause);
    }
}
