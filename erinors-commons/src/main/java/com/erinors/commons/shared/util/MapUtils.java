/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

import java.util.HashMap;
import java.util.Map;

public class MapUtils
{
    private MapUtils()
    {
    }

    @SuppressWarnings("unchecked")
    public static <S, T> Map<S, T> create(Object... args)
    {
        Map<S, T> map = new HashMap<S, T>();

        for (int i = 0; i < args.length; i += 2)
        {
            map.put((S) args[i], (T) args[i + 1]);
        }

        return map;
    }

    public static Map<String, String> createTranslationMap(Object... translations)
    {
        if (translations.length % 2 != 0)
        {
            throw new IllegalArgumentException("Language-translation pairs expected but got odd number of arguments: "
                    + translations);
        }

        Map<String, String> translationMap = new HashMap<String, String>(translations.length / 2);
        for (int i = 0; i < translations.length; i += 2)
        {
            Object languageDefinition = translations[i];
            String language;
            if (languageDefinition instanceof String)
            {
                language = (String) languageDefinition;
            }
            else
            {
                throw new IllegalArgumentException("Locale definition should be String  but got " + languageDefinition);
            }

            Object translationDefinition = translations[i + 1];
            String translation;
            if (translationDefinition instanceof String)
            {
                translation = (String) translationDefinition;
            }
            else
            {
                throw new IllegalArgumentException("Translation definition should be a String but got "
                        + translationDefinition);
            }

            translationMap.put(language, translation);
        }

        return translationMap;
    }
}
