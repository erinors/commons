/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

public class ToStringBuilder
{
    private final StringBuilder buffer = new StringBuilder();

    private final String objectName;

    public ToStringBuilder(Object object)
    {
        if (object == null)
        {
            throw new IllegalArgumentException("Non-null object expected.");
        }

        this.objectName = object.getClass().getName();
    }

    public ToStringBuilder(String objectName)
    {
        if (objectName == null)
        {
            throw new IllegalArgumentException("Non-null object name expected.");
        }

        this.objectName = objectName;
    }

    private void appendComma()
    {
        if (buffer.length() > 0)
        {
            buffer.append(", ");
        }
    }

    public ToStringBuilder append(String name, Object value)
    {
        appendComma();

        buffer.append(name);
        buffer.append("=");
        buffer.append(value);

        return this;
    }

    @Override
    public String toString()
    {
        return objectName + "(" + buffer + ")";
    }
}
