/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

public class StringUtils
{
    public static final String EMPTY = "";

    public static boolean isNullOrEmpty(String string)
    {
        return isNullOrEmpty(string, false);
    }

    public static boolean isNullOrEmpty(String string, boolean trim)
    {
        if (string == null)
        {
            return true;
        }

        if (trim)
        {
            string = string.trim();
        }

        return string.isEmpty();
    }

    public static String leftPad(String string, int length, char c)
    {
        if (string == null)
        {
            throw new IllegalArgumentException("Non-null string expected.");
        }

        if (string.length() >= length)
        {
            return string;
        }

        StringBuilder b = new StringBuilder(length);

        int padding = length - string.length();
        for (int i = 0; i < padding; i++)
        {
            b.append(c);
        }

        b.append(string);

        return b.toString();
    }

    public static String repeat(String str, int repeat)
    {
        if (str == null)
        {
            return null;
        }

        if (repeat <= 0)
        {
            return EMPTY;
        }

        int inputLength = str.length();
        if (repeat == 1 || inputLength == 0)
        {
            return str;
        }

        StringBuilder b = new StringBuilder();
        for (int i = 0; i < repeat; i++)
        {
            b.append(str);
        }

        return b.toString();
    }

    private StringUtils()
    {
    }
}
