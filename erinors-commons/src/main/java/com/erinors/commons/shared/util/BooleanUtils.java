/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

public final class BooleanUtils
{
    /**
     * Check if exactly one boolean value is true.
     */
    public static boolean checkIfExactlyOneItemIsTrue(boolean... values)
    {
        assert values != null;

        if (values.length == 0)
        {
            throw new IllegalArgumentException("At least 1 value is expected.");
        }

        int trueCount = 0;
        for (boolean value : values)
        {
            if (value)
            {
                if (trueCount < 1)
                {
                    trueCount++;
                }
                else
                {
                    return false;
                }
            }
        }

        return trueCount == 1;
    }

    private BooleanUtils()
    {
    }
}
