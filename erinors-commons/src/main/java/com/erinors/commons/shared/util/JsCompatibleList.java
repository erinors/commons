/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

import java.util.Iterator;
import java.util.List;

public interface JsCompatibleList
{
    void add(Object element);

    int size();

    Object get(int index);

    boolean getBoolean(int index);

    byte getByte(int index);

    char getChar(int index);

    double getDouble(int index);

    float getFloat(int index);

    int getInt(int index);

    long getLong(int index);

    short getShort(int index);

    List<Object> asList();
    
    Iterator<Object> iterator();
}
