/*
 * #%L
 * erinors-commons
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of erinors-commons.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.commons.shared.util;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Pair<First, Second> implements Serializable
{
    private First first;

    private Second second;

    public Pair()
    {
    }

    public Pair(First first, Second second)
    {
        this.first = first;
        this.second = second;
    }

    public First getFirst()
    {
        return first;
    }

    public void setFirst(First first)
    {
        this.first = first;
    }

    public Second getSecond()
    {
        return second;
    }

    public void setSecond(Second second)
    {
        this.second = second;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(first).append(second).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Pair<?, ?>))
        {
            return false;
        }

        Pair<?, ?> p = (Pair<?, ?>) obj;
        return new EqualsBuilder().append(first, p.first).append(second, p.second).isEquals();
    }

    @Override
    public String toString()
    {
        return "pair(" + first + ", " + second + ")";
    }
}
