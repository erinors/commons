/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.processor;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementScanner6;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import com.erinors.jsonserializer.shared.JsonRpcUtils;
import com.erinors.jsonserializer.shared.annotation.JsonRpcServlet;

@SupportedAnnotationTypes(value = { "*" })
@SupportedSourceVersion(SourceVersion.RELEASE_6)
public class RpcServletAnnotationProcessor extends AbstractProcessor
{
    private static final String CLASS_NAME_JSONSERIALIZER_REMOTESERVICE = "com.erinors.jsonserializer.shared.JsonRemoteService";

    private static final String TAB = "    ";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {
        final Elements elementUtils = processingEnv.getElementUtils();
        final Types typeUtils = processingEnv.getTypeUtils();

        //
        // Check environment
        //

        TypeElement jsonRemoteServiceTypeElement = elementUtils.getTypeElement(CLASS_NAME_JSONSERIALIZER_REMOTESERVICE);
        if (jsonRemoteServiceTypeElement == null)
        {
            return false;
        }

        //
        // Processing
        //

        for (Element rootElement : roundEnv.getRootElements())
        {
            if (rootElement.getKind() == ElementKind.INTERFACE)
            {
                final TypeElement typeElement = (TypeElement) rootElement;

                JsonRpcServlet rpcServletAnnotation = typeElement.getAnnotation(JsonRpcServlet.class);

                if (rpcServletAnnotation != null)
                {
                    TypeMirror type = typeElement.asType();
                    String qualifiedName = typeElement.getQualifiedName().toString();

                    boolean jsonService = typeUtils.isSubtype(type, jsonRemoteServiceTypeElement.asType());
                    if (!jsonService)
                    {
                        return false;
                    }

                    //
                    // Determine servlet name
                    //

                    String rpcServletClassName = null;
                    if (!rpcServletAnnotation.className().isEmpty())
                    {
                        rpcServletClassName = rpcServletAnnotation.className();
                    }
                    else
                    {
                        int modulePackageIndex = qualifiedName.indexOf(".client.");

                        if (modulePackageIndex == -1)
                        {
                            modulePackageIndex = qualifiedName.indexOf(".shared.");
                        }

                        if (modulePackageIndex == -1)
                        {
                            processingEnv.getMessager().printMessage(
                                    Kind.ERROR,
                                    "The class name of the generated servlet cannot be determined automatically, please specify it explicitly by @"
                                            + JsonRpcServlet.class.getName() + ".className.", typeElement);

                            return false;
                        }
                        else
                        {
                            rpcServletClassName = qualifiedName.substring(0, modulePackageIndex)
                                    + ".server.servlet.rpc.json." + typeElement.getSimpleName() + "Servlet";
                        }
                    }

                    String rpcServletMapping;
                    if (rpcServletAnnotation.mapping().isEmpty())
                    {
                        rpcServletMapping = rpcServletClassName;

                        processingEnv.getMessager().printMessage(
                                Kind.NOTE,
                                "It is recommended to use an explicit servlet mapping name using @"
                                        + JsonRpcServlet.class.getName()
                                        + ".mappingName. The generated default mapping name is: " + rpcServletMapping,
                                typeElement);
                    }
                    else
                    {
                        rpcServletMapping = rpcServletAnnotation.mapping();
                    }

                    //
                    // Generate servlet source code
                    //

                    try
                    {
                        JavaFileObject javaFileObject = processingEnv.getFiler().createSourceFile(rpcServletClassName,
                                typeElement);
                        OutputStream outputStream = null;
                        try
                        {
                            outputStream = javaFileObject.openOutputStream();
                            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                            //
                            // Package declaration
                            //

                            writer.println("package "
                                    + rpcServletClassName.substring(0, rpcServletClassName.lastIndexOf('.')) + ";");
                            writer.println();

                            //
                            // @WebServlet annotation
                            //

                            if (rpcServletAnnotation.addWebServletAnnotation())
                            {
                                writer.print("@javax.servlet.annotation.WebServlet(");

                                writer.print("name = \"");
                                writer.print(rpcServletClassName);
                                writer.print("\", urlPatterns=\"/");

                                writer.print(JsonRpcUtils.RpcDefaultPathFragment);
                                writer.print(rpcServletMapping);
                                writer.print("\"");

                                writer.println(")");
                            }

                            //
                            // Class declaration
                            //

                            writer.println("public class "
                                    + rpcServletClassName.substring(rpcServletClassName.lastIndexOf('.') + 1)
                                    + " extends " + "com.erinors.jsonserializer.servlet.JsonServlet" + " implements "
                                    + qualifiedName);
                            writer.println("{");

                            writer.println(TAB + "protected " + qualifiedName + " getDelegate()");
                            writer.println(TAB + "{");
                            writer.print(TAB
                                    + TAB
                                    + "return ("
                                    + qualifiedName
                                    + ") org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).");
                            writer.print("getBean(" + qualifiedName + ".class)");
                            writer.println(";");
                            writer.println(TAB + "}");

                            generateServletBody(writer, typeElement);

                            writer.println("}");

                            writer.flush();
                        }
                        finally
                        {
                            if (outputStream != null)
                            {
                                outputStream.close();
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

        return false;
    }

    private void generateServletBody(final PrintWriter writer, final TypeElement typeElement)
    {
        writer.println();
        writer.println(TAB + "@" + Override.class.getSimpleName());
        writer.println(TAB + "@" + SuppressWarnings.class.getSimpleName() + "(\"unchecked\")");

        writer.println(TAB
                + "protected final void invokeMethod(String methodName, final java.util.Map<String, String> arguments, java.io.PrintWriter writer)");
        writer.println(TAB + "{");

        writer.println(TAB + TAB + "String returnValue;");

        typeElement.accept(new ElementScanner6<Void, Void>()
        {
            private boolean firstIf = true;

            @Override
            public Void visitExecutable(ExecutableElement e, Void p)
            {
                if (e.getKind() == ElementKind.METHOD && typeElement.equals(e.getEnclosingElement()))
                {
                    String methodName = e.getSimpleName().toString();
                    boolean isVoid = e.getReturnType().getKind() == TypeKind.VOID;

                    writer.print(TAB + TAB);

                    if (firstIf)
                    {
                        firstIf = false;
                    }
                    else
                    {
                        writer.print("else ");
                    }

                    writer.print("if (\"");
                    writer.print(methodName);
                    writer.println("\".equals(methodName))");
                    writer.println(TAB + TAB + "{");

                    writer.print(TAB + TAB + TAB
                            + "returnValue = executeTask(new com.erinors.commons.shared.util.Function<String>()");
                    writer.println(TAB + TAB + TAB + "{");
                    writer.println(TAB + TAB + TAB + TAB + "public String apply()");
                    writer.println(TAB + TAB + TAB + TAB + "{");

                    writer.print(TAB + TAB + TAB + TAB);
                    if (!isVoid)
                    {
                        writer.print("return getJsonSerializer().serialize(");
                    }
                    writer.println();

                    writer.println(TAB + TAB + TAB + TAB + TAB + "getDelegate()." + methodName + "(");

                    int i = 0;
                    for (VariableElement parameter : e.getParameters())
                    {
                        if (i++ > 0)
                        {
                            writer.print(", ");
                        }

                        writer.println(TAB + TAB + TAB + TAB + TAB + TAB + TAB
                                + "getJsonSerializer().deserialize(arguments.get(\"" + parameter.getSimpleName()
                                + "\"), " + parameter.asType() + ".class)");
                    }

                    writer.print(TAB + TAB + TAB + TAB + TAB + TAB + ")");

                    if (isVoid)
                    {
                        writer.print(";");
                    }
                    writer.println();

                    if (isVoid)
                    {
                        writer.println(TAB + TAB + TAB + TAB + TAB + "return null;");
                    }
                    else
                    {
                        writer.println(TAB + TAB + TAB + TAB + TAB + ");");
                    }

                    writer.println(TAB + TAB + TAB + TAB + "}");
                    writer.println(TAB + TAB + TAB + "}, false);");

                    writer.println(TAB + TAB + "}");
                }

                return null;
            }
        }, null);

        writer.println(TAB + TAB + "else {");
        writer.println(TAB + TAB + TAB + "throw new RuntimeException(\"Invalid method name: \" + methodName);");
        writer.println(TAB + TAB + "}");
        writer.println(TAB + "}");
    }
}
