/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

@SuppressWarnings("serial")
public class JsonParseException extends RuntimeException
{
    public JsonParseException(String message, String json, int currentIndex, Throwable cause)
    {
        super(message, cause);
    }

    public JsonParseException(String message, String json, int currentIndex)
    {
        this(message + " Position: " + currentIndex + ", fragment: '"
                + json.substring(currentIndex, Math.min(json.length(), 32)) + "'", json, currentIndex, null);
    }
}
