/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

import java.io.Reader;
import java.io.Writer;

public interface JsonSerializer
{
    void registerSerializer(TypeSerializer<?> serializer, int order);

    void registerDefaultSerializers();

    String serialize(Object object);

    void serialize(Object object, Writer writer);

    Object deserialize(String json);
    
    <T> T deserialize(String json, Class<T> targetClass);

    <T> T deserialize(Reader jsonReader, Class<T> targetClass);
}
