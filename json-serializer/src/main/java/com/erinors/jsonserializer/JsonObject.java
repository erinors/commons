/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

import java.util.HashMap;
import java.util.Map;

import com.erinors.commons.shared.util.MapUtils;

public class JsonObject
{
    private Map<String, Object> properties = new HashMap<String, Object>();

    public JsonObject()
    {
    }

    public JsonObject(Object... propertyDefinitions)
    {
        // TODO check types
        properties.putAll(MapUtils.<String, Object> create(propertyDefinitions));
    }

    public JsonObject property(String name, Object value)
    {
        setProperty(name, value);
        return this;
    }

    public void setProperty(String name, Object value)
    {
        properties.put(name, value);
    }

    public Map<?, ?> getProperties()
    {
        return properties;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProperty(String name)
    {
        return (T) getProperties().get(name);
    }

    public String getPropertyString(String name)
    {
        return getProperty(name);
    }

    public Number getPropertyNumber(String name)
    {
        return getProperty(name);
    }

    public Integer getPropertyInteger(String name)
    {
        Number number = getPropertyNumber(name);
        return number != null ? number.intValue() : null;
    }

    public Long getPropertyLong(String name)
    {
        Number number = getPropertyNumber(name);
        return number != null ? number.longValue() : null;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof JsonObject))
        {
            return false;
        }

        JsonObject other = (JsonObject) obj;
        return properties.equals(other.properties);
    }

    @Override
    public int hashCode()
    {
        return properties.hashCode();
    }

    @Override
    public String toString()
    {
        return properties.toString(); // TODO use writer
    }
}
