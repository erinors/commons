/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

import com.erinors.commons.shared.util.EqualsBuilder;
import com.erinors.commons.shared.util.HashCodeBuilder;


public final class JsonFunction
{
    private final String parameters;

    private final String code;

    public JsonFunction(String parameters, String code)
    {
        this.parameters = parameters;
        this.code = code;
    }

    public String getParameters()
    {
        return parameters;
    }

    public String getCode()
    {
        return code;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof JsonFunction))
        {
            return false;
        }

        JsonFunction other = (JsonFunction) obj;
        return new EqualsBuilder().append(parameters, other.parameters).append(code, other.code).isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(parameters).append(code).toHashCode();
    }
}
