/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.plugin.activator;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator
{
    public static final String PLUGIN_ID = "com.erinors.json-serializer";

    private static Activator plugin;

    public Activator()
    {
    }

    public void start(BundleContext context) throws Exception
    {
        plugin = this;
    }

    public void stop(BundleContext context) throws Exception
    {
        plugin = null;
    }

    public static Activator getDefault()
    {
        return plugin;
    }
}
