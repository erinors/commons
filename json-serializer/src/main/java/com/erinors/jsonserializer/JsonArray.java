/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JsonArray implements Iterable<Object>
{
    private List<Object> list = new LinkedList<Object>();

    public JsonArray()
    {
    }

    public JsonArray(Object... elements)
    {
        for (Object element : elements)
        {
            add(element);
        }
    }

    public JsonArray add(Object element)
    {
        list.add(element);
        return this;
    }

    @Override
    public Iterator<Object> iterator()
    {
        return list.iterator();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof JsonArray))
        {
            return false;
        }

        JsonArray other = (JsonArray) obj;
        return list.equals(other.list);
    }

    @Override
    public int hashCode()
    {
        return list.hashCode();
    }

    @Override
    public String toString()
    {
        return list.toString(); // TODO use jsonwriter
    }
}
