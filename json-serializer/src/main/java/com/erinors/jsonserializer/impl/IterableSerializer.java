/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import java.util.Iterator;

import com.erinors.jsonserializer.ArrayIterator;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.TypeSerializer;

public class IterableSerializer implements TypeSerializer<Iterable<?>>
{
    public static final int ORDER = 128;

    @Override
    public boolean supports(Class<?> clazz)
    {
        return Iterable.class.isAssignableFrom(clazz);
    }

    @Override
    public void serialize(final Iterable<?> iterable, JsonWriter jsonWriter)
    {
        jsonWriter.write(new ArrayIterator()
        {
            private final Iterator<?> it = iterable.iterator();

            public Object next()
            {
                return it.next();
            }

            public boolean hasNext()
            {
                return it.hasNext();
            }
        });
    }
}
