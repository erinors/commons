/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.erinors.commons.shared.util.Pair;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.ObjectIterator;
import com.erinors.jsonserializer.TypeSerializer;

public class MapSerializer implements TypeSerializer<Map<?, ?>>
{
    public static final int ORDER = 128;
    
    @Override
    public boolean supports(Class<?> clazz)
    {
        return Map.class.isAssignableFrom(clazz);
    }

    @Override
    public void serialize(final Map<?, ?> map, JsonWriter jsonWriter)
    {
        jsonWriter.write(new ObjectIterator()
        {
            private final Iterator<?> it = map.entrySet().iterator();

            public Pair<String, ?> next()
            {
                Entry<?, ?> entry = (Entry<?, ?>) it.next();

                Object key = entry.getKey();
                if (key == null)
                {
                    throw new IllegalStateException("Null key found: " + map);
                }

                Object value = entry.getValue();

                return new Pair<String, Object>(key.toString(), value);
            }

            public boolean hasNext()
            {
                return it.hasNext();
            }
        });
    }
}
