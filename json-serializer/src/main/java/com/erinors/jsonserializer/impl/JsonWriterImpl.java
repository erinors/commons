/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import com.erinors.commons.shared.util.Pair;
import com.erinors.jsonserializer.ArrayIterator;
import com.erinors.jsonserializer.JsonFunction;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.ObjectIterator;
import com.erinors.jsonserializer.TypeSerializer;

public class JsonWriterImpl implements JsonWriter
{
    private final List<Pair<Integer, TypeSerializer<?>>> serializers;

    private final PrintWriter writer;

    private final boolean formatted = true; // TODO configurable

    private int indentation;

    private int level;

    public JsonWriterImpl(List<Pair<Integer, TypeSerializer<?>>> serializers, Writer writer)
    {
        this.serializers = serializers;
        this.writer = new PrintWriter(writer);
    }

    public void write(Object object)
    {
        try
        {
            level++;
            checkLevel();

            if (object == null)
            {
                writeNull();
            }
            else
            {
                Class<?> objectClass = object.getClass();
                TypeSerializer<?> serializer = null;
                for (Pair<Integer, TypeSerializer<?>> entry : serializers)
                {
                    TypeSerializer<?> typeSerializer = entry.getSecond();
                    if (typeSerializer.supports(objectClass))
                    {
                        serializer = typeSerializer;
                        break;
                    }
                }

                if (serializer != null)
                {
                    ((TypeSerializer<Object>) serializer).serialize(object, this);
                }
                else
                {
                    throw new IllegalStateException("No serializer found for " + object);
                }
            }
        }
        finally
        {
            level--;
        }
    }

    private void checkLevel()
    {
        if (level > 100) // TODO configurable
        {
            throw new IllegalStateException("Stack overflow.");
        }
    }

    public void writeNull()
    {
        writer.append("null");
    }

    public void write(boolean value)
    {
        writer.append(Boolean.toString(value));
    }

    public void write(Number number)
    {
        writer.append(number.toString());
    }

    public void write(ArrayIterator arrayIterator)
    {
        try
        {
            level++;
            checkLevel();

            writer.append("[");

            if (formatted)
            {
                writer.println();
            }

            indentation++;

            boolean first = true;
            while (arrayIterator.hasNext())
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    writer.append(",");

                    if (formatted)
                    {
                        writer.println();
                    }
                }

                indent();
                write(arrayIterator.next());
            }

            indentation--;

            if (formatted)
            {
                writer.println();
            }

            indent();
            writer.append("]");
        }
        finally
        {
            level--;
        }
    }

    private void indent()
    {
        if (formatted)
        {
            int i = indentation;
            while (i-- > 0)
            {
                writer.print("    "); // TODO tabs?
            }
        }
    }

    public void write(ObjectIterator objectIterator)
    {
        try
        {
            level++;
            checkLevel();

            writer.append("{");

            if (formatted)
            {
                writer.println();
            }

            indentation++;

            boolean first = true;
            while (objectIterator.hasNext())
            {
                Pair<String, ?> next = objectIterator.next();

                if (next.getSecond() == null)
                {
                    continue; // TODO customizable
                }

                if (first)
                {
                    first = false;
                }
                else
                {
                    writer.append(",");

                    if (formatted)
                    {
                        writer.println();
                    }
                }

                indent();
                write(next.getFirst());

                if (formatted)
                {
                    writer.print(' ');
                }

                writer.print(':');

                if (formatted)
                {
                    writer.print(' ');
                }

                write(next.getSecond());
            }

            indentation--;

            if (formatted)
            {
                writer.println();
            }

            indent();
            writer.append("}");
        }
        finally
        {
            level--;
        }
    }

    public void write(String string)
    {
        writer.print('\"');
        writer.print(string); // TODO encode special chars
        writer.print('\"');
    }

    public void write(JsonFunction jsonFunction)
    {
        writer.print("function(");
        writer.print(jsonFunction.getParameters());
        writer.println(") {");
        writer.println(jsonFunction.getCode());
        writer.print("}");
    }
}
