/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.erinors.commons.shared.util.Pair;
import com.erinors.jsonserializer.JsonSerializer;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.TypeSerializer;

public class JsonSerializerImpl implements JsonSerializer
{
    private final List<Pair<Integer, TypeSerializer<?>>> serializers = new ArrayList<Pair<Integer, TypeSerializer<?>>>();

    @Override
    public void registerSerializer(TypeSerializer<?> serializer, int order)
    {
        Pair<Integer, TypeSerializer<?>> entry = new Pair<Integer, TypeSerializer<?>>(order, serializer);
        serializers.add(entry);

        Collections.sort(serializers, new Comparator<Pair<Integer, TypeSerializer<?>>>()
        {
            @Override
            public int compare(Pair<Integer, TypeSerializer<?>> o1, Pair<Integer, TypeSerializer<?>> o2)
            {
                return o1.getFirst().compareTo(o2.getFirst());
            }
        });
    }

    public String serialize(Object object)
    {
        StringWriter writer = new StringWriter();
        serialize(object, writer);
        return writer.toString();
    }

    @Override
    public void serialize(Object object, Writer writer)
    {
        JsonWriter jsonWriter = new JsonWriterImpl(new ArrayList<Pair<Integer, TypeSerializer<?>>>(serializers), writer);
        jsonWriter.write(object);
    }

    public void registerDefaultSerializers()
    {
        registerSerializer(new NumberSerializer(), NumberSerializer.ORDER);
        registerSerializer(new StringSerializer(), StringSerializer.ORDER);
        registerSerializer(new BooleanSerializer(), BooleanSerializer.ORDER);
        registerSerializer(new JsonObjectSerializer(), JsonObjectSerializer.ORDER);
        registerSerializer(new IterableSerializer(), IterableSerializer.ORDER);
        registerSerializer(new MapSerializer(), MapSerializer.ORDER);
        registerSerializer(new EnumSerializer(), EnumSerializer.ORDER);
        registerSerializer(new ReflectionSerializer(), ReflectionSerializer.ORDER);
        registerSerializer(new JsonFunctionSerializer(), JsonFunctionSerializer.ORDER);
    }

    @Override
    public <T> T deserialize(String json, Class<T> targetClass)
    {
        return deserialize(new StringReader(json), targetClass);
    }

    @Override
    public <T> T deserialize(Reader jsonReader, Class<T> targetClass)
    {
        return null;
    }

    @Override
    public Object deserialize(String json)
    {
        return null;
    }
}
