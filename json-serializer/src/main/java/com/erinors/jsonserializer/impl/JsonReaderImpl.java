/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import com.erinors.jsonserializer.JsonArray;
import com.erinors.jsonserializer.JsonObject;
import com.erinors.jsonserializer.JsonParseException;
import com.erinors.jsonserializer.JsonReader;

public class JsonReaderImpl implements JsonReader
{
    private static final String TOKEN_NULL = "null";

    private static final String TOKEN_TRUE = "true";

    private static final String TOKEN_FALSE_START = "fals";

    private static final char TOKEN_FALSE_END = 'e';

    private final String jsonString;

    private final char[] json;

    private final int jsonLength;

    private int currentIndex;

    public JsonReaderImpl(String json)
    {
        this.jsonString = json;
        this.json = json.toCharArray();
        jsonLength = json.length();
    }

    private boolean hasMoreChars()
    {
        return currentIndex < jsonLength;
    }

    private char readChar()
    {
        if (!hasMoreChars())
        {
            throw new JsonParseException("End of JSON.", jsonString, currentIndex);
        }

        char c = lookupChar();
        consumeChar();
        return c;
    }

    private String readChars(int count)
    {
        if (currentIndex + count > jsonLength)
        {
            throw new JsonParseException("End of JSON.", jsonString, currentIndex);
        }

        String chars = new String(json, currentIndex, count);

        currentIndex += count;

        return chars;
    }

    private char lookupChar()
    {
        return json[currentIndex];
    }

    private void consumeChar()
    {
        currentIndex++;
    }

    private void consumeWhiteSpaces()
    {
        char c;
        while (hasMoreChars())
        {
            c = lookupChar();

            if (Character.isWhitespace(c))
            {
                consumeChar();
            }
            else
            {
                break;
            }
        }
    }

    @Override
    public Object readValue()
    {
        Object value;

        consumeWhiteSpaces();

        char c = lookupChar();

        switch (c)
        {
        // String
        case '\"':
            value = readStringStrict();
            break;

        // Number
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            value = readNumberStrict();
            break;

        // Object
        case '{':
            value = readObjectStrict();
            break;

        // Array
        case '[':
            value = readArrayStrict();
            break;

        // Boolean
        case 't':
        case 'f':
            value = readBooleanStrict();
            break;

        // Null
        case 'n':
            value = readNull();
            break;

        default:
            throw new JsonParseException(
                    "Invalid value, one of string, number, object, array, boolean or null expected.", jsonString,
                    currentIndex);
        }

        return value;
    }

    private <T> T readNull()
    {
        consumeWhiteSpaces();

        String token = readChars(4);

        if (!TOKEN_NULL.equals(token))
        {
            throw new JsonParseException("Expected null but found: " + token, jsonString, currentIndex);
        }

        return null;
    }

    @Override
    public Boolean readBoolean()
    {
        consumeWhiteSpaces();

        char c = lookupChar();

        Boolean result;

        switch (c)
        {
        case 'n':
            result = readNull();
            break;

        case 't':
        case 'f':
            result = readBooleanStrict();
            break;

        default:
            throw new JsonParseException("Expected 'true', 'false' or 'null'.", jsonString, currentIndex);
        }

        return result;
    }

    private boolean readBooleanStrict()
    {
        assert hasMoreChars() && !Character.isWhitespace(lookupChar());
        // TODO assert lookupChar() == ...

        int tokenIndex = currentIndex;
        String token = readChars(4);

        if (TOKEN_TRUE.equals(token))
        {
            return true;
        }
        else if (TOKEN_FALSE_START.equals(token))
        {
            char c = readChar();

            if (c != TOKEN_FALSE_END)
            {
                throw new JsonParseException("Expected 'true' or 'false' but found: " + token, jsonString, tokenIndex);
            }

            return false;
        }
        else
        {
            throw new JsonParseException("Expected 'true' or 'false' but found: " + token, jsonString, tokenIndex);
        }
    }

    @Override
    public String readString()
    {
        consumeWhiteSpaces();

        char c = lookupChar();

        String result;

        switch (c)
        {
        case 'n':
            result = readNull();
            break;

        case '\"':
            result = readStringStrict();
            break;

        default:
            throw new JsonParseException("Expected string or 'null'.", jsonString, currentIndex);
        }

        return result;
    }

    private String readStringStrict()
    {
        assert hasMoreChars() && !Character.isWhitespace(lookupChar());
        // TODO assert lookupChar() == ...

        readChar();

        StringBuilder buffer = new StringBuilder();
        Loop: do
        {
            char c = readChar();

            switch (c)
            {
            case '\"':
                break Loop;
            case '\\':
                char control = readChar();

                switch (control)
                {
                case '\"':
                case '\\':
                case '/':
                    buffer.append(control);
                    break;
                case 'b':
                    buffer.append('\b');
                    break;
                case 'f':
                    buffer.append('\f');
                    break;
                case 'n':
                    buffer.append('\n');
                    break;
                case 'r':
                    buffer.append('\r');
                    break;
                case 't':
                    buffer.append('\t');
                    break;
                case 'u':
                    String codeString = readChars(4);

                    int code;
                    try
                    {
                        code = Integer.parseInt(codeString, 16);
                    }
                    catch (Exception e)
                    {
                        throw new JsonParseException("Invalid character code: " + control, jsonString, currentIndex - 4);
                    }

                    buffer.append((char) code);
                    break;
                default:
                    throw new JsonParseException("Unexpected control character: " + control, jsonString,
                            currentIndex - 1);
                }
                break;
            default:
                buffer.append(c);
                break;
            }
        }
        while (true);

        return buffer.toString();
    }

    @Override
    public Double readNumber()
    {
        consumeWhiteSpaces();

        char c = lookupChar();

        Double result;

        switch (c)
        {
        case 'n':
            result = readNull();
            break;

        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            result = readNumberStrict();
            break;

        default:
            throw new JsonParseException("Expected number or 'null'.", jsonString, currentIndex);
        }

        return result;
    }

    private double readNumberStrict()
    {
        assert hasMoreChars() && !Character.isWhitespace(lookupChar());
        // TODO assert lookupChar() == ...

        int tokenIndex = currentIndex;

        StringBuilder buffer = new StringBuilder();

        // TODO more correct parser

        Loop: do
        {
            if (!hasMoreChars())
            {
                break;
            }

            char c = lookupChar();

            switch (c)
            {
            case '+':
            case '-':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '.':
            case 'e':
            case 'E':
                readChar();
                buffer.append(c);
                break;

            default:
                break Loop;
            }
        }
        while (true);

        double number;
        try
        {
            number = Double.parseDouble(buffer.toString());
        }
        catch (Exception e)
        {
            throw new JsonParseException("Expected number but found: " + buffer, jsonString, tokenIndex);
        }

        return number;
    }

    @Override
    public JsonArray readArray()
    {
        consumeWhiteSpaces();

        char c = lookupChar();

        JsonArray result;

        switch (c)
        {
        case 'n':
            result = readNull();
            break;

        case '[':
            result = readArrayStrict();
            break;

        default:
            throw new JsonParseException("Expected array or 'null'.", jsonString, currentIndex);
        }

        return result;
    }

    private JsonArray readArrayStrict()
    {
        assert hasMoreChars() && !Character.isWhitespace(lookupChar());

        assert lookupChar() == '[';
        readChar();

        JsonArray array = new JsonArray();

        consumeWhiteSpaces();

        if (lookupChar() == ']')
        {
            readChar();
        }
        else
        {
            Loop: do
            {
                Object element = readValue();
                array.add(element);

                consumeWhiteSpaces();

                char c = readChar();
                switch (c)
                {
                case ',':
                    break;
                case ']':
                    break Loop;
                default:
                    throw new JsonParseException("Expected ',' or ']'.", jsonString, currentIndex);
                }
            }
            while (true);
        }

        return array;
    }

    @Override
    public JsonObject readObject()
    {
        consumeWhiteSpaces();

        char c = lookupChar();

        JsonObject result;

        switch (c)
        {
        case 'n':
            result = readNull();
            break;

        case '{':
            result = readObjectStrict();
            break;

        default:
            throw new JsonParseException("Expected object or 'null'.", jsonString, currentIndex);
        }

        return result;
    }

    private JsonObject readObjectStrict()
    {
        assert hasMoreChars() && !Character.isWhitespace(lookupChar());

        assert lookupChar() == '{';
        readChar();

        JsonObject object = new JsonObject();

        if (lookupChar() == '}')
        {
            readChar();
        }
        else
        {
            Loop: do
            {
                consumeWhiteSpaces();
                String name = readStringStrict();

                consumeWhiteSpaces();
                if (readChar() != ':')
                {
                    throw new JsonParseException("Expected ':'.", jsonString, currentIndex);
                }

                Object value = readValue();

                object.setProperty(name, value);

                consumeWhiteSpaces();
                char c = readChar();
                switch (c)
                {
                case ',':
                    break;
                case '}':
                    break Loop;
                default:
                    throw new JsonParseException("Expected ',' or '}'.", jsonString, currentIndex);
                }
            }
            while (true);
        }

        return object;
    }
}
