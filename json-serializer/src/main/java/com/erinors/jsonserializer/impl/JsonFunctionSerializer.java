/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import com.erinors.jsonserializer.JsonFunction;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.TypeSerializer;

public class JsonFunctionSerializer implements TypeSerializer<JsonFunction>
{
    public static final int ORDER = 128;

    @Override
    public boolean supports(Class<?> clazz)
    {
        return JsonFunction.class.isAssignableFrom(clazz);
    }

    @Override
    public void serialize(JsonFunction value, JsonWriter jsonWriter)
    {
        jsonWriter.write(value);
    }
}
