/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.erinors.commons.shared.util.Pair;
import com.erinors.jsonserializer.JsonWriter;
import com.erinors.jsonserializer.ObjectIterator;
import com.erinors.jsonserializer.TypeSerializer;

// TODO remove, add JavaBeanSerializer instead
public class ReflectionSerializer implements TypeSerializer<Object>
{
    public static final int ORDER = 1024;
    
    private Map<Class<?>, Iterable<Field>> fieldCache = new HashMap<Class<?>, Iterable<Field>>();

    @Override
    public boolean supports(Class<?> clazz)
    {
        return true;
    }

    @Override
    public void serialize(final Object object, JsonWriter jsonWriter)
    {
        jsonWriter.write(new ObjectIterator()
        {
            private final Iterator<Field> it = getFields(object.getClass()).iterator();

            public Pair<String, ?> next()
            {
                Field field = it.next();

                Object value;
                try
                {
                    value = field.get(object);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Cannot read field: " + field, e);
                }

                Object key = field.getName(); // TODO handle collision

                return new Pair<String, Object>(key.toString(), value);
            }

            public boolean hasNext()
            {
                return it.hasNext();
            }
        });
    }

    // TODO cache
    protected Iterable<Field> getFields(Class<?> clazz)
    {
        Iterable<Field> fields = fieldCache.get(clazz);

        if (fields == null)
        {
            List<Field> fieldList = new ArrayList<Field>();
            addFields(clazz, fieldList);

            fields = fieldList;
        }

        return fields;
    }

    private void addFields(Class<?> clazz, List<Field> fieldList)
    {
        if (clazz.getSuperclass() != null)
        {
            addFields(clazz.getSuperclass(), fieldList);
        }

        for (Field field : clazz.getDeclaredFields())
        {
            field.setAccessible(true);
            fieldList.add(field);
        }
    }
}
