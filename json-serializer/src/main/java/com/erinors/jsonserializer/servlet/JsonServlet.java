/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationContextUtils;

import com.erinors.commons.shared.util.Function1;
import com.erinors.jsonserializer.JsonSerializer;

/**
 * <p>
 * Base class for automatically generated JSON RPC servlet classes.
 * </p>
 * <p>
 * Subclasses are similar to:
 * 
 * <pre>
 * interface JsonService extends JsonRemoteService
 * {
 *     Foo load(long fooId);
 * 
 *     &#064;JsonConversion(transactional = true)
 *     void save(Foo foo);
 * }
 * 
 * class JsonServletSubclass extends JsonServlet
 * {
 *     protected final String invokeMethod(String methodName, Map&lt;String, String&gt; arguments)
 *     {
 *         String returnValue;
 *         
 *         if (&quot;load&quot;.equals(methodName))
 *         {
 *             returnValue = executeTask(new Function1&lt;String, Object&gt;()
 *             {
 *                 public String apply(Object param1)
 *                 {
 *                     return getJsonSerializer().serialize(
 *                         getDelegate().load(
 *                             getJsonSerializer().deserialize(arguments.get(&quot;fooId&quot;), long.class)
 *                             );
 *                         );
 *                 }
 *             }, false);
 *         }
 *         else if (&quot;save&quot;.equals(methodName))
 *         {
 *             returnValue = executeTask(new Function1&lt;String, Object&gt;()
 *             {
 *                 public String apply(Object param1)
 *                 {
 *                     getDelegate().save(
 *                         getJsonSerializer().deserialize(arguments.get(&quot;foo&quot;), Foo.class)
 *                         );
 *                     return null;
 *                 }
 *             }, true);
 *         }
 *         else
 *         {
 *             throw new RuntimeException(&quot;Invalid method name: &quot; + methodName);
 *         }
 *         
 *         return returnValue;
 *     }
 * }
 * </pre>
 * 
 * </p>
 */
@SuppressWarnings("serial")
public abstract class JsonServlet extends HttpServlet
{
    protected JsonSerializer getJsonSerializer()
    {
        // TODO configurable
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext()).getBean(
                JsonSerializer.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        processRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        String methodName = extractMethodName(request.getPathInfo());
        Map<String, String> arguments = extractArguments(request.getParameterMap());

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        invokeMethod(methodName, arguments, response.getWriter());
    }

    protected Map<String, String> extractArguments(Map<String, String[]> parameterMap)
    {
        Map<String, String> arguments = new HashMap<String, String>();

        for (Entry<String, String[]> entry : parameterMap.entrySet())
        {
            String name = entry.getKey();

            if (entry.getValue() == null || entry.getValue().length != 1)
            {
                throw new RuntimeException("Invalid argument, exactly 1 value per parameter expected: " + name + "="
                        + Arrays.toString(entry.getValue()));
            }

            arguments.put(name, entry.getValue()[0]);
        }

        return arguments;
    }

    protected String extractMethodName(String pathInfo)
    {
        if (pathInfo == null)
        {
            throw new RuntimeException("Service method name is not specified.");
        }

        assert pathInfo.startsWith("/");
        String methodName = pathInfo.substring(1);

        if (methodName.contains("/"))
        {
            throw new RuntimeException("Invalid service method name: " + methodName);
        }

        return methodName;
    }

    protected String executeTask(Function1<String, Map<String, String>> task, Map<String, String> argument, boolean transactional)
    {
        return task.apply(argument); // TODO transactional
    }

    protected abstract void invokeMethod(String methodName, Map<String, String> arguments, PrintWriter writer);
}
