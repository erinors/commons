package com.erinors.jsonserializer;

@SuppressWarnings("serial")
public class JsonDeserializationException extends RuntimeException
{
    public JsonDeserializationException(String message)
    {
        super(message);
    }

    public JsonDeserializationException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
