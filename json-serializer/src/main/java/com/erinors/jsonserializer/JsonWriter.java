/*
 * #%L
 * json-serializer
 * %%
 * Copyright (C) 2012 Erinors
 * %%
 * This file is part of json-serializer.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * #L%
 */

package com.erinors.jsonserializer;

public interface JsonWriter
{
    void writeNull();

    void write(Object object);

    void write(boolean value);

    void write(Number number);

    void write(String string);

    void write(ArrayIterator arrayIterator);

    void write(ObjectIterator objectIterator);
    
    void write(JsonFunction jsonFunction);
}
