package com.erinors.jsonserializer.reader;

import static org.junit.Assert.*;
import org.junit.Test;

import com.erinors.jsonserializer.JsonArray;
import com.erinors.jsonserializer.JsonObject;
import com.erinors.jsonserializer.impl.JsonReaderImpl;

public class JsonReaderReadValueTest
{
    private Object read(String json)
    {
        return new JsonReaderImpl(json).readValue();
    }

    @Test
    public void testNull()
    {
        assertNull(read("null"));
    }

    @Test
    public void testString()
    {
        assertEquals("", read("\"\""));
        assertEquals("a", read("\"a\""));
        assertEquals("123456", read("\"123456\""));
        assertEquals("\"", read("\"\\\"\""));
        // TODO more
    }

    @Test
    public void testNumber()
    {
        assertEquals(0.0, read("0"));
        assertEquals(1.0, read("1"));
        assertEquals(-135.65, read("-135.65"));
    }

    @Test
    public void testBoolean()
    {
        assertEquals(true, read("true"));
        assertEquals(false, read("false"));
    }

    @Test
    public void testArray()
    {
        assertEquals(new JsonArray(), read("[]"));
        assertEquals(new JsonArray(1.0, 2.0, 3.0), read("[1,2,3]"));
        assertEquals(new JsonArray("a", "b", "c"), read("[\"a\",\"b\",\"c\"]"));
        assertEquals(new JsonArray("a", 2.1, "c", 4.0), read("[\"a\",2.1,\"c\",4]"));
    }

    @Test
    public void testObject()
    {
        assertEquals(new JsonObject(), read("{}"));
        assertEquals(new JsonObject("a", 1.0), read("{\"a\":1}"));
    }

    @Test
    public void testComplex()
    {
        assertEquals(
                new JsonObject("array", new JsonArray(1.0, 2.0, 3.0), "nested", new JsonObject("a", "x", "b", 2.0)),
                read("{\"array\":[1,2,3],\"nested\":{\"a\":\"x\",\"b\":2}}"));
    }
}
