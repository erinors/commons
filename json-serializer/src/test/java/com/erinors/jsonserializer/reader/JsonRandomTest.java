package com.erinors.jsonserializer.reader;

import org.junit.Test;

import com.erinors.jsonserializer.JsonSerializer;
import com.erinors.jsonserializer.impl.JsonSerializerImpl;
import com.erinors.jsonserializer.util.JsonGenerator;

public class JsonRandomTest
{
    @Test
    public void test()
    {
        JsonSerializer serializer = new JsonSerializerImpl();
        serializer.registerDefaultSerializers();

        JsonGenerator jsonGenerator = new JsonGenerator();

        Object object = jsonGenerator.generateObject();
        System.out.println(serializer.serialize(object));
    }
}
