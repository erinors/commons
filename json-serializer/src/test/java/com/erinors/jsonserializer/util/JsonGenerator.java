package com.erinors.jsonserializer.util;

import java.util.Random;

import com.erinors.jsonserializer.JsonArray;
import com.erinors.jsonserializer.JsonObject;

/*
 * Some parts of the code below is partially derived from: http://code.google.com/p/json-test-suite/
 */
public final class JsonGenerator
{
    private static final char[] SPECIALS = { '\"', '\\', '/' };

    private static final int STRING_LENGTH_MAXIMUM = 100;

    private static final int ARRAY_LENGTH_MAX = 30;

    private static final int OBJECT_PROPERTY_COUNT_MAX = 20;

    private static final int COMPOSITE_OBJECT_COUNT_MAX = 10;

    private enum ValueType
    {
        NULL(5), BOOLEAN(5), STRING(20), NUMBER(20), ARRAY(20), OBJECT(30);

        private int distribution;

        ValueType(int distribution)
        {
            this.distribution = distribution;
        }

        public int getDistribution()
        {
            return distribution;
        }
    }

    private static final ValueType[] distributionArray;

    static
    {
        int setSize = 0;
        for (ValueType valueType : ValueType.values())
        {
            setSize += valueType.getDistribution();
        }

        distributionArray = new ValueType[setSize];

        int i = 0;
        for (ValueType valueType : ValueType.values())
        {
            for (int j = 0; j < valueType.getDistribution(); j++)
            {
                distributionArray[i++] = valueType;
            }
        }
    }

    private final Random random = new Random();

    public JsonGenerator()
    {
    }

    public Object generateObject()
    {
        return generateObject(0);
    }

    public Object generateObject(int currentObjectCount)
    {
        if (currentObjectCount > COMPOSITE_OBJECT_COUNT_MAX)
        {
            return null;
        }

        switch (distributionArray[random.nextInt(distributionArray.length)])
        {
        case ARRAY:
            JsonArray array = new JsonArray();

            for (int i = 0; i < random.nextInt(ARRAY_LENGTH_MAX); i++)
            {
                array.add(generateObject(currentObjectCount + 1));
            }

            return array;

        case OBJECT:
            JsonObject object = new JsonObject();

            for (int i = 0; i < random.nextInt(OBJECT_PROPERTY_COUNT_MAX); i++)
            {
                object.setProperty(generateString(), generateObject(currentObjectCount + 1));
            }

            return object;

        case BOOLEAN:
            return random.nextBoolean();

        case NULL:
            return null;

        case NUMBER:
            double value = random.nextDouble();
            long fractor = random.nextLong();

            if (random.nextBoolean() && fractor != 0)
            {
                value *= fractor;
            }
            else
            {
                value /= fractor;
            }

            if (random.nextBoolean())
            {
                return value;
            }
            else
            {
                return -1 * value;
            }

        case STRING:
            return generateString();
        }

        throw new IllegalStateException();
    }

    private char generateControl()
    {
        return (char) random.nextInt('\u0020');
    }

    private char generateSpecial()
    {
        return SPECIALS[random.nextInt(SPECIALS.length)];
    }

    private char generatePrintable()
    {
        char ch = (char) ('\u0020' + random.nextInt('\u007f' - '\u0020' + 1));
        return ch;
    }

    private char generateOther()
    {
        return (char) random.nextInt(Character.MAX_VALUE + 1);
    }

    private String generateString()
    {
        int len = random.nextInt(STRING_LENGTH_MAXIMUM + 1);
        StringBuffer sb = new StringBuffer();

        /*
         * Special and control characters are relatively less than printable characters in real world.
         */
        for (int i = 0; i < len; i++)
        {
            char ch;
            int t = random.nextInt(82);
            switch (t)
            {
            case 0:
                ch = generateControl();
                break;
            case 1:
                ch = generateSpecial();
                /*
                 * JSON RI only espace '/' when it follows '<'. Simulate this special case.
                 */
                if (ch == '/' && i < len - 1)
                {
                    sb.append('<');
                    i++;
                }
                break;
            default:
                if (t < 41)
                    ch = generatePrintable();
                else
                    ch = generateOther();
            }
            sb.append(ch);
        }

        return sb.toString();
    }
}
